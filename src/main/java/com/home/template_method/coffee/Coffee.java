package com.home.template_method.coffee;

public class Coffee {

    private CoffeeType coffeeType;
    private Milk milk;
    private Liqueur liqueur;
    private OtherAdditives otherAdditives;

    public Coffee(CoffeeType coffeeType, Milk milk, Liqueur liqueur, OtherAdditives otherAdditives) {
        this.coffeeType = coffeeType;
        this.milk = milk;
        this.liqueur = liqueur;
        this.otherAdditives = otherAdditives;
    }

    public Coffee() {

    }

    public CoffeeType getCoffeeType() {
        return coffeeType;
    }

    public void setCoffeeType(CoffeeType coffeeType) {
        this.coffeeType = coffeeType;
    }

    public Milk getMilk() {
        return milk;
    }

    public void setMilk(Milk milk) {
        this.milk = milk;
    }

    public Liqueur getLiqueur() {
        return liqueur;
    }

    public void setLiqueur(Liqueur liqueur) {
        this.liqueur = liqueur;
    }

    public OtherAdditives getOtherAdditives() {
        return otherAdditives;
    }

    public void setOtherAdditives(OtherAdditives otherAdditives) {
        this.otherAdditives = otherAdditives;
    }
}

enum CoffeeType {
    ARABICA, ROBUSTA;
}

enum Milk {
    FAT, REDUCED_FAT;
}

enum Liqueur {
    SCOTCH_WHISKY, BAILEYS;
}

enum OtherAdditives {
    SUGAR, SALT;
}
