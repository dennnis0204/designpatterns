package com.home.template_method.coffee;

public class CoffeeMakerTest {
    public static void main(String[] args) {

        Coffee irishCoffee = new IrishCoffeeMaker().makeCoffee();
        Coffee espressoCoffee = new EspressoCoffeeMaker().makeCoffee();
    }
}
