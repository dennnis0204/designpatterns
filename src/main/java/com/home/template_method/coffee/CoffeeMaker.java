package com.home.template_method.coffee;

public abstract class CoffeeMaker {

    public final Coffee makeCoffee() {
        Coffee coffee = new Coffee();
        addCoffee(coffee);
        addMilk(coffee);
        addLiqueur(coffee);
        addAdditives(coffee);
        return coffee;
    }

    abstract Coffee addCoffee(Coffee coffee);
    abstract Coffee addMilk(Coffee coffee);
    abstract Coffee addLiqueur(Coffee coffee);
    abstract Coffee addAdditives(Coffee coffee);
}
