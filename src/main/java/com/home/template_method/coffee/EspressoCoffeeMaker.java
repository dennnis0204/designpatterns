package com.home.template_method.coffee;

public class EspressoCoffeeMaker extends CoffeeMaker {
    @Override
    Coffee addCoffee(Coffee coffee) {
        coffee.setCoffeeType(CoffeeType.ROBUSTA);
        return coffee;
    }

    @Override
    Coffee addMilk(Coffee coffee) {
        return coffee;
    }

    @Override
    Coffee addLiqueur(Coffee coffee) {
        return coffee;
    }

    @Override
    Coffee addAdditives(Coffee coffee) {
        coffee.setOtherAdditives(OtherAdditives.SUGAR);
        return coffee;
    }
}
