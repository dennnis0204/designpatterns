package com.home.template_method.coffee;

public class IrishCoffeeMaker extends CoffeeMaker {

    @Override
    Coffee addCoffee(Coffee coffee) {
        coffee.setCoffeeType(CoffeeType.ARABICA);
        return coffee;
    }

    @Override
    Coffee addMilk(Coffee coffee) {
        coffee.setMilk(Milk.FAT);
        return coffee;
    }

    @Override
    Coffee addLiqueur(Coffee coffee) {
        coffee.setLiqueur(Liqueur.SCOTCH_WHISKY);
        return coffee;
    }

    @Override
    Coffee addAdditives(Coffee coffee) {
        coffee.setOtherAdditives(OtherAdditives.SUGAR);
        return coffee;
    }
}
