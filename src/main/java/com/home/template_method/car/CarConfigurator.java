package com.home.template_method.car;

public abstract class CarConfigurator {

    public final Car configureCar() {
        Car car = new Car(
                configureEngine(),
                configureNavigation(),
                configureWheelSize(),
                configureCruiseControl());
        return car;
    }

    public abstract String configureEngine();

    public abstract boolean configureNavigation();

    public abstract int configureWheelSize();

    public abstract String configureCruiseControl();
}
