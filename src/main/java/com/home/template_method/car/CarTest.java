package com.home.template_method.car;

public class CarTest {

    public static void main(String[] args) {
        Car toyota = new ToyotaCarConfigurator().configureCar();
        Car nissan = new NissanCarConfigurator().configureCar();
    }
}
