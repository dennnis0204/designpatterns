package com.home.template_method.car;

public class Car {

    private String engine;
    private boolean hasNavigation;
    private int wheelSize;
    private String cruiseControl;

    public Car(String engine, boolean hasNavigation, int wheelSize, String cruiseControl) {
        this.engine = engine;
        this.hasNavigation = hasNavigation;
        this.wheelSize = wheelSize;
        this.cruiseControl = cruiseControl;
    }
}
