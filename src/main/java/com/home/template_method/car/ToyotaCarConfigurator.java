package com.home.template_method.car;

public class ToyotaCarConfigurator extends CarConfigurator {
    @Override
    public String configureEngine() {
        return "Hybrid";
    }

    @Override
    public boolean configureNavigation() {
        return true;
    }

    @Override
    public int configureWheelSize() {
        return 17;
    }

    @Override
    public String configureCruiseControl() {
        return "Semi-autonomous cruise control";
    }
}
