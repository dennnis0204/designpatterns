package com.home.template_method.car;

public class NissanCarConfigurator extends CarConfigurator {

    @Override
    public String configureEngine() {
        return "Diesel 2000";
    }

    @Override
    public boolean configureNavigation() {
        return true;
    }

    @Override
    public int configureWheelSize() {
        return 20;
    }

    @Override
    public String configureCruiseControl() {
        return "Adaptive Cruise Control";
    }
}
