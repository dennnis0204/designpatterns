package com.home.builder.pizza;

public enum PizzaType {
    NEAPOLITAN_PIZZA, DETROIT_PIZZA, SICILIAN_PIZZA;
}
