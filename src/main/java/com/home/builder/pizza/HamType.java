package com.home.builder.pizza;

public enum HamType {
    COUNTRY_HAM, CITY_HAM, GLAZED_HAM;
}
