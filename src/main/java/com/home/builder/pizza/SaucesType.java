package com.home.builder.pizza;

public enum SaucesType {
    TOMATO_SAUCE, MUSTARD_SAUCE;
}
