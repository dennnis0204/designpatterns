package com.home.builder.pizza;

import java.util.LinkedList;
import java.util.List;

public class PizzaChef implements Chef {

    private PizzaType pizzaType;
    private PizzaSize pizzaSize;
    private String cake;
    private String cheese;
    private int cheeseAmount;
    private List<SaucesType> saucesList = new LinkedList<>();
    private List<Vegetable> vegetables = new LinkedList<>();
    private SalamiType salamiType;
    private HamType hamType;

    @Override
    public PizzaChef reset() {
        this.saucesList.clear();
        this.vegetables.clear();
        return this;
    }

    @Override
    public PizzaChef setPizzaType(PizzaType pizzaType) {
        this.pizzaType = pizzaType;
        return this;
    }

    @Override
    public PizzaChef setPizzaSize(PizzaSize pizzaSize) {
        this.pizzaSize = pizzaSize;
        return this;
    }

    @Override
    public PizzaChef setAmountOfCheese(int cheeseAmount) {
        this.cheeseAmount = cheeseAmount;
        return this;
    }

    @Override
    public PizzaChef addHam(HamType hamType) {
        this.hamType = hamType;
        return this;
    }

    @Override
    public PizzaChef addSalami(SalamiType salamiType) {
        this.salamiType = salamiType;
        return this;
    }

    @Override
    public PizzaChef addMushrooms() {
        this.vegetables.add(Vegetable.MUSHROOM);
        return this;
    }

    @Override
    public PizzaChef addTomatoes() {
        this.vegetables.add(Vegetable.TOMATO);
        return this;
    }

    @Override
    public PizzaChef addCorn() {
        this.vegetables.add(Vegetable.CORN);
        return this;
    }

    @Override
    public PizzaChef addAdditionalSauces(SaucesType saucesType) {
        this.saucesList.add(saucesType);
        return this;
    }

    @Override
    public PizzaChef addChiliPeppers() {
        this.vegetables.add(Vegetable.CHILI_PEPPER);
        return this;
    }

    public Pizza bakePizza() {
        return new Pizza(pizzaType, pizzaSize, "Yeast Dough", "Mozzarella", cheeseAmount, saucesList, vegetables, salamiType, hamType);
    }
}
