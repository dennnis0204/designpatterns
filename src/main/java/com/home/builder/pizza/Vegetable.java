package com.home.builder.pizza;

public enum Vegetable {
    CORN, CHILI_PEPPER, MUSHROOM, TOMATO;
}
