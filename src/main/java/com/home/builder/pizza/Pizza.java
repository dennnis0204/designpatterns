package com.home.builder.pizza;

import java.util.LinkedList;
import java.util.List;

public class Pizza {

    private PizzaType pizzaType;
    private PizzaSize pizzaSize;
    private String cake;
    private String cheese;
    private int cheeseAmount;
    private List<SaucesType> saucesList = new LinkedList<>();
    private List<Vegetable> vegetables = new LinkedList<>();
    private SalamiType salamiType;
    private HamType hamType;

    public Pizza(PizzaType pizzaType,
                 PizzaSize pizzaSize,
                 String cake,
                 String cheese,
                 int cheeseAmount,
                 List<SaucesType> saucesList,
                 List<Vegetable> vegetables,
                 SalamiType salamiType,
                 HamType hamType) {
        this.pizzaType = pizzaType;
        this.pizzaSize = pizzaSize;
        this.cake = cake;
        this.cheese = cheese;
        this.cheeseAmount = cheeseAmount;
        this.saucesList = saucesList;
        this.vegetables = vegetables;
        this.salamiType = salamiType;
        this.hamType = hamType;
    }

    public PizzaType getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(PizzaType pizzaType) {
        this.pizzaType = pizzaType;
    }

    public PizzaSize getPizzaSize() {
        return pizzaSize;
    }

    public void setPizzaSize(PizzaSize pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    public String getCake() {
        return cake;
    }

    public void setCake(String cake) {
        this.cake = cake;
    }

    public String getCheese() {
        return cheese;
    }

    public void setCheese(String cheese) {
        this.cheese = cheese;
    }

    public int getCheeseAmount() {
        return cheeseAmount;
    }

    public void setCheeseAmount(int cheeseAmount) {
        this.cheeseAmount = cheeseAmount;
    }

    public List<SaucesType> getSaucesList() {
        return saucesList;
    }

    public void setSaucesList(List<SaucesType> saucesList) {
        this.saucesList = saucesList;
    }

    public List<Vegetable> getVegetables() {
        return vegetables;
    }

    public void setVegetables(List<Vegetable> vegetables) {
        this.vegetables = vegetables;
    }

    public SalamiType getSalamiType() {
        return salamiType;
    }

    public void setSalamiType(SalamiType salamiType) {
        this.salamiType = salamiType;
    }

    public HamType getHamType() {
        return hamType;
    }

    public void setHamType(HamType hamType) {
        this.hamType = hamType;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "pizzaType=" + pizzaType +
                ", pizzaSize=" + pizzaSize +
                ", cake='" + cake + '\'' +
                ", cheese='" + cheese + '\'' +
                ", cheeseAmount=" + cheeseAmount +
                ", saucesList=" + saucesList +
                ", vegetables=" + vegetables +
                ", salamiType=" + salamiType +
                ", hamType=" + hamType +
                '}';
    }
}
