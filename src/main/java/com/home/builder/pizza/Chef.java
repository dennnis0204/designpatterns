package com.home.builder.pizza;

public interface Chef {
    PizzaChef reset();
    PizzaChef setPizzaType(PizzaType pizzaType);
    PizzaChef setPizzaSize(PizzaSize pizzaSize);
    PizzaChef setAmountOfCheese(int cheeseAmount);
    PizzaChef addHam(HamType hamType);
    PizzaChef addSalami(SalamiType salamiType);
    PizzaChef addMushrooms();
    PizzaChef addTomatoes();
    PizzaChef addCorn();
    PizzaChef addAdditionalSauces(SaucesType saucesType);
    PizzaChef addChiliPeppers();
}
