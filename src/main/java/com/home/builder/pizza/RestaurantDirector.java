package com.home.builder.pizza;

public class RestaurantDirector {

    public void bakeDetroitPizza(Chef chef, PizzaSize pizzaSize) {
        chef
                .reset()
                .setPizzaType(PizzaType.DETROIT_PIZZA)
                .setPizzaSize(pizzaSize)
                .setAmountOfCheese(125)
                .addHam(HamType.GLAZED_HAM)
                .addTomatoes()
                .addMushrooms()
                .addAdditionalSauces(SaucesType.TOMATO_SAUCE);
    }

    public void bakeNeapolitanPizza(Chef chef, PizzaSize pizzaSize) {
        chef
                .reset()
                .setPizzaType(PizzaType.NEAPOLITAN_PIZZA)
                .setPizzaSize(pizzaSize)
                .setAmountOfCheese(165)
                .addSalami(SalamiType.WINTER_SALAMI)
                .addCorn()
                .addMushrooms()
                .addAdditionalSauces(SaucesType.TOMATO_SAUCE);
    }

    public void bakeSicilianPizza(Chef chef, PizzaSize pizzaSize) {
        chef
                .reset()
                .setPizzaType(PizzaType.SICILIAN_PIZZA)
                .setPizzaSize(pizzaSize)
                .setAmountOfCheese(75)
                .addHam(HamType.CITY_HAM)
                .addTomatoes()
                .addCorn()
                .addAdditionalSauces(SaucesType.TOMATO_SAUCE)
                .addAdditionalSauces(SaucesType.MUSTARD_SAUCE)
                .addChiliPeppers();
    }
}
