package com.home.builder.pizza;

public class PizzaTest {

    public static void main(String[] args) {
        RestaurantDirector restaurantDirector = new RestaurantDirector();
        PizzaChef pizzaChef = new PizzaChef();

        restaurantDirector.bakeDetroitPizza(pizzaChef, PizzaSize.MEDIUM);
        Pizza detroitPizza = pizzaChef.bakePizza();
        System.out.println(detroitPizza);

        restaurantDirector.bakeNeapolitanPizza(pizzaChef, PizzaSize.LARGE);
        Pizza neapolitanPizza = pizzaChef.bakePizza();
        System.out.println(neapolitanPizza);

        restaurantDirector.bakeSicilianPizza(pizzaChef, PizzaSize.SMALL);
        Pizza sicilianPizza = pizzaChef.bakePizza();
        System.out.println(sicilianPizza);
    }
}
