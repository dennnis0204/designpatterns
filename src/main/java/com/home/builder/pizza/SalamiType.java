package com.home.builder.pizza;

public enum SalamiType {
    PEPPERONI, WINTER_SALAMI;
}
