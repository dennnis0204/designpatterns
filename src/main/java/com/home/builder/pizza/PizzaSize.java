package com.home.builder.pizza;

public enum PizzaSize {
    SMALL, MEDIUM, LARGE;
}
