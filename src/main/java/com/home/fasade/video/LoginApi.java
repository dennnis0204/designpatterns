package com.home.fasade.video;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LoginApi {
    private Map<String, String> registeredUsers = new HashMap<>();
    private List<String> loggedInUsers = new LinkedList<>();

    public boolean registerNewUser(String login, String password) {
        if (registeredUsers.get(login) == null) {
            registeredUsers.put(login, password);
            return true;
        }
        return false;
    }

    public boolean login(String login, String password) {
        String thePassword = registeredUsers.get(login);
        if (thePassword != null && thePassword.equals(password)) {
            loggedInUsers.add(login);
            return true;
        }
        return false;
    }

    public boolean logout(String login) {
        return loggedInUsers.remove(login);
    }

    public boolean isSignedIn(String login) {
        return loggedInUsers.contains(login);
    }
}
