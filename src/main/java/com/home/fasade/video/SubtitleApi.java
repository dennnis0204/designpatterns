package com.home.fasade.video;

public class SubtitleApi {

    public void enableCC() {
        System.out.println("Subtitles are enabled");
    }

    public void disableCC() {
        System.out.println("Subtitles are disabled");
    }
}
