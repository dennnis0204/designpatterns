package com.home.fasade.video;

public class VideoApi {
    public void playVideoByUrl(String url) {
        System.out.println("You are watching video from url: " + url);
    }
}
