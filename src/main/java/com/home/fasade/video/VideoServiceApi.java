package com.home.fasade.video;

public class VideoServiceApi {

    LoginApi loginApi = new LoginApi();
    SubtitleApi subtitleApi = new SubtitleApi();
    VideoApi videoApi = new VideoApi();

    public boolean register(String login, String password) {
        return loginApi.registerNewUser(login, password);
    }

    public boolean login(String login, String password) {
        return loginApi.login(login, password);
    }

    public boolean logout(String login) {
        return loginApi.logout(login);
    }

    public boolean isSignedIn(String login) {
        return loginApi.isSignedIn(login);
    }

    public void enableCC() {
        subtitleApi.enableCC();
    }

    public void disableCC() {
        subtitleApi.disableCC();
    }

    public void playVideoByUrl(String url) {
        videoApi.playVideoByUrl(url);
    }
}
