package com.home.fasade.video;

public class VideoServiceTest {
    public static void main(String[] args) {
        VideoServiceApi videoService = new VideoServiceApi();
        User user = new User("john88", "sky55");
        if (videoService.register(user.getLogin(), user.getPassword())) {
            System.out.println(user.getLogin() + " is registered");
        };
        if (videoService.login(user.getLogin(), user.getPassword())) {
            System.out.println(user.getLogin() + " is logged in");
        }
        if (videoService.isSignedIn(user.getLogin())) {
            videoService.playVideoByUrl("http://youtube.com");
        }
        videoService.logout(user.getLogin());
        videoService.enableCC();
    }
}

class User {
    private String login;
    private String password;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
