package com.home.fasade.shop_api;

import java.util.List;

public class ShopAPI {

    private PaymentAPI paymentAPI;
    private ProductAPI productAPI;
    private UserAPI userApi;

    public ShopAPI() {
        this.paymentAPI = new PaymentAPI();
        this.productAPI = new ProductAPI();
        this.userApi = new UserAPI();
    }

    void register(String login, String password) {
        userApi.register(login, password);
    }

    void login(String login, String password) {
        userApi.login(login, password);
    }

    void logout() {
        userApi.logout();
    }

    void buyProducts(float amount) {
        paymentAPI.pay(amount);
    }

    void addProduct(Product product) {
        productAPI.addProduct(product);
    }

    void removeProduct(Product product) {
        productAPI.removeProduct(product);
    }

    List<Product> getAllProducts() {
        return productAPI.getProducts();
    }

    Product getProductByName(String productName) {
        return productAPI.getProductByName(productName);
    }

}
