package com.home.fasade.shop_api;

import java.util.List;

public class ShopAPITest {
    public static void main(String[] args) {
        ShopAPI shop = new ShopAPI();
        shop.login("admin", "password");
        shop.addProduct(new Product("bicycle", "yellow", 255));
        shop.addProduct(new Product("tent", "large", 133));
        List<Product> productList = shop.getAllProducts();
        for (Product product : productList) {
            System.out.println(product);
        }
        Product product = shop.getProductByName("bicycle");
        System.out.println(product);
    }
}
