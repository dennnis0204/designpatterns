package com.home.fasade.shop_api;

import java.util.*;

public class ProductAPI {
    private List<Product> productList = new LinkedList<>();

    List<Product> getProducts() {
        return productList;
    }

    Product getProductByName(String name) {
        return productList
                .stream()
                .filter(product -> product.getName() == name)
                .findFirst()
                .get();
    }

    void addProduct(Product product) {
        productList.add(product);
    };

    void removeProduct(Product product) {
        productList.remove(product);
    };
}
