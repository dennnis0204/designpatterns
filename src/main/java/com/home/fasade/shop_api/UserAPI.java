package com.home.fasade.shop_api;

public class UserAPI {
    void register(String login, String password){
        System.out.println("You are registered");
    };
    void login(String login, String password){
        System.out.println("You are logged in");
    };
    void logout(){
        System.out.println("You are logged out");
    };
}
